# Lassie

*A friendly earthquake detector.*

## Installation

First, install [Pyrocko](http://pyrocko.org/current/install.html),
then install Lassie:

```bash
git clone https://git.pyrocko.org/pyrocko/lassie.git
cd lassie
pip3 install . --no-deps  # assuming pyrocko is already installed
```

## Tutorial: detecting events in a regional network

This tutorial explains how to use Lassie to detect events in a regional
network. The one-day test dataset from the Alentejo region in South Portugal
can be downloaded from
http://data.pyrocko.org/examples/lassie-example-alentejo.tar (235 MB). Data
from 12 seismic stations from the
[DOCTAR](http://www.geo.uni-potsdam.de/doctar-1317/articles/doctar-1317.html)
experiment and from 5 additional stations (networks [PM][1], [WM][2] and
[8A][3]) are included. A region of roughly 100 x 100 km is covered by the
available data.

[1]: <https://doi.org/10.7914/SN/PM> "Instituto Português do Mar e da Atmosfera, I.P. (2006). Portuguese National Seismic Network [Data set]. International Federation of Digital Seismograph Networks."
[2]: <https://doi.org/10.14470/JZ581150> "San Fernando Royal Naval Observatory (ROA), Universidad Complutense De Madrid (UCM), Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum (GFZ), Universidade De Évora (UEVORA, Portugal), & Institute Scientifique Of Rabat (ISRABAT, Morocco). (1996). The Western Mediterranean BB seismic Network [Data set]. Deutsches GeoForschungsZentrum GFZ."
[3]: <https://doi.org/10.14470/3N7565750319> "Dias, N. A., Silveira, G., & Haberland, C. (2010). Data of the temporary seismic WILAS network [Data set]. GFZ Data Services."

#### Dataset preparation

```bash
# download data and unpack
wget https://data.pyrocko.org/examples/lassie-example-alentejo.tar
tar -xf lassie-example-alentejo.tar
cd lassie-example-alentejo
ls
# data/                 - raw waveforms in mseed format
# stations.txt          - text file with station coordinates
# confirmed-events.txt  - catalog with some known events
```

To get a quick overview on the dataset, use the
[Snuffler](https://pyrocko.org/docs/current/apps/snuffler/)
application which is part of [Pyrocko](https://pyrocko.org/docs/current/index.html)
([tutorial](https://pyrocko.org/docs/current/apps/snuffler/tutorial.html)).

```bash
snuffler --stations=stations.txt --events=confirmed-events.txt data/
```

#### Hints, when preparing your own dataset

* NET.STA.LOC codes in waveform files must match exactly what is given in the
  stations file.

#### Running lassie from the command line

The Lassie earthquake detector can be run from the command line or by calling
Lassie's library functions from a Python script. Here we will demonstrate the
use of the `lassie` command line tool. To get brief help on available command
line options, run `lassie --help` or `lassie <subcommand> --help`. Once dataset
and configuration are ready, the command `lassie search <configfile>` runs the
main detector algorithm. But first it must be configured...

#### Lassie configuration

Lassie reads its configuration from a text file in the
[YAML](https://en.wikipedia.org/wiki/YAML) format. To create an initial
configuration, run

```bash
lassie init > config.yaml
# or filling in already the names of the stations file and the waveform directory:
lassie init --stations=stations.txt --data=data > config.yaml
```


```yaml
%YAML 1.1
--- !lassie.Config

## Configuration file for Lassie, your friendly earthquake detector
##
## Receiver coordinates can be read from a stations file in Pyrocko format:
stations_path: 'stations.txt'

## Receivers can also be listed in the config file, lat/lon and carthesian
## (x/y/z) = (North/East/Down) coordinates are supported and may be combined
## (interpreted as reference + offset). Omitted values are treated as zero.
# receivers:
# - !lassie.Receiver
#   codes: ['', 'ACC13', '']
#   lat: 10.
#   lon: 12.
#   x: 2397.56
#   y: 7331.94
#   z: -404.1

## List of data directories. Lassie will recurse into subdirectories to find
## all contained waveform files.
data_paths:
- 'data'

## Processing time interval (default: use time interval of available data)
# tmin: '2012-02-06 04:20:00'
# tmax: '2012-02-06 04:30:00'

## Search grid; if not given here (if commented), a default grid will be chosen
# grid: !lassie.Carthesian3DGrid
#   lat: 38.7
#   lon: -7.9
#   xmin: -70e3  # in [m]
#   xmax: 70e3
#   ymin: -70e3
#   ymax: 70e3
#   zmin: 0.0
#   zmax: 0.0
#   dx: 2.5e3
#   dy: 2.5e3
#   dz: 2.5e3

## Size factor to use when automatically choosing a grid size
autogrid_radius_factor: 1.5

## Grid density factor used when automatically choosing a grid
autogrid_density_factor: 10.0

## Composition of image function
image_function_contributions:
- !lassie.WavePacketIFC
  name: 'P'
  weight: 1.0
  fmin: 1.0
  fmax: 15.0
  fsmooth_factor: 0.1
  shifter: !lassie.VelocityShifter
    velocity: 3500.
- !lassie.WavePacketIFC
  name: 'S'
  weight: 1.0
  fmin: 1.0
  fmax: 10.0
  fsmooth_factor: 0.1
  shifter: !lassie.VelocityShifter
    velocity: 2500.

## Whether to divide image function frames by their mean value
sharpness_normalization: true

## Threshold on detector function
detector_threshold: 100.0

## Output filename for detections
detections_path: 'detections.txt'
```

Make sure, that the entries for `data_paths` and `stations_path` point to the
respective directory and file.

An example event from the dataset is shown here:

![example waveforms](doc/gx/alentejo-event1.png "'Event 1' from 'confirmed-events.txt'")

Typical regional, shallow earthquakes appear in this dataset as clear P and S
coda wave packets travelling with characteristic speeds, packet durations and
frequency content through the network. To detect such events automatically, we
can compose an image function with two contributions, one for each dominant
phase. Such image function contributions (IFCs) can be defined under the
`image_function_contributions` key in Lassie's configuration. Different types
of IFCs are available and can be combined. Here, we use the `WavePacketIFC`
which applies a normalized envelope based pre-processing to the waveforms
before travel-time compensated stacking.

#### Running the detector

To test the configuration, select a short processing time span around one of
the known events in the dataset. The time span can be restricted with the
`tmin` and `tmax` entries in Lassie's configuration or with corresponding
command line options `--tmin='YYYY-MM-DD HH:MM:SS'` and `--tmax='YYYY-MM-DD
HH:MM:SS.XXX'`, which override configuration settings.

Now run the detector with

```bash
lassie search config.yaml
```

#### Scrutinize detections

After the detection has finished have a look at the results using Pyrocko's
[Snuffler](https://pyrocko.org/docs/current/apps/snuffler/):

```bash
lassie snuffle config.yaml
```

![example snuffling](doc/gx/snuffling_example.png "Test with synthetic events")

Snuffler opens loading waveform data together with station meta data and
detections as event markers. Load a reference catalog to compare the detections
to and scrutinize detection performance at different detection thresholds,
interactively.


