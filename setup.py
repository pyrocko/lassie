import os
import sys
from pkg_resources import parse_version as pv
from setuptools import __version__ as setuptools_version, setup

have_pep621_support = pv(setuptools_version) >= pv('61.0.0')

if have_pep621_support:
    setup()

else:

    # Fallback for setuptools < 61.0.0. This is needed to continue native
    # install support (pip install --no-deps --no-build-isolation .) for e.g.
    # - Debian 10 (setuptools 40.8.0, EOL June 30, # 2024)
    # - Debian 11 (setuptools 52.0.0, expected EOL June, 2026)

    try:
        import toml
    except ImportError:
        sys.exit(
            '''Your setuptools version is too old to support PEP621-compliant
            installs. You may either update setuptools or, if this is not
            possible, install the 'toml' package (python3-toml package on
            deb-based systems) to enable a fallback mechanism.'''
        )

    tomldata = toml.load(
        open(os.path.join(os.path.dirname(__file__), 'pyproject.toml'))
    )

    metadata = dict(
        package_dir=tomldata['tool']['setuptools']['package-dir'],
        # newer versions of setuptools include package data automatically
        # based on MANIFEST.in, however older, e.g. 40.8.0 (Debian 10) do not.
        # Can't find out when this is needed and when not.
        package_data={'lassie': ['data/*.wav']},
    )
    metadata['packages'] = ['lassie', 'lassie.apps', 'lassie.data']

    for k in ['name', 'version', 'description', 'classifiers', 'keywords']:
        if k in tomldata['project']:
            metadata[k] = tomldata['project'][k]

    metadata['license'] = tomldata['project']['license']['text']

    metadata['python_requires'] = tomldata['project']['requires-python']
    first_author = list(tomldata['project']['authors'])[0]
    metadata['author'] = ', '.join(
        author['name'] for author in tomldata['project']['authors']
    )
    metadata['author_email'] = first_author['email']

    metadata['extras_require'] = {}
    for k_opt in tomldata['project']['optional-dependencies']:
        metadata['extras_require'][k_opt] = tomldata['project'][
            'optional-dependencies'
        ][k_opt]

    metadata['install_requires'] = tomldata['project']['dependencies']
    metadata['entry_points'] = {
        'console_scripts': [
            '%s = %s' % (k, v)
            for (k, v) in tomldata['project']['scripts'].items()
        ],
    }

    setup(**metadata)
