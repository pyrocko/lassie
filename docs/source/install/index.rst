Installation Instructions
=========================

Lassie depends on `pyrocko <http://pyrocko.org>`_, see the `install instructions <http://pyrocko.org/v0.3/install/>`_ for details.


Debian based systems
--------------------

.. code-block :: sh

    git clone https://github.com/pyrocko/lassie
    cd grond
    sudo python setup.py install
