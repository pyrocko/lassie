.. grond documentation master file, created by
   sphinx-quickstart on Fri Jun 23 11:40:19 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Lassie - The friendly earthquake detector's Documentation!
===========================================================

.. toctree::
   install/index
   library/index
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
